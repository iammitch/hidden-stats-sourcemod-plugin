#include <sourcemod>

#define PLUGIN_VERSION "1.4.2"

// Query types.
#define QUERY_TYPE_GLOBAL 1
#define QUERY_TYPE_COUNTRY 2
#define QUERY_TYPE_SERVER 3
#define QUERY_TYPE_CLUSTER 4

#define QUERY_CMD_RANK "rank"
#define QUERY_CMD_STATS "stats"
#define QUERY_CMD_TIMEPLAYED "timeplayed"

// Database
new Handle:hndl_database;

// cVars...
new Handle:cvar_ServerId;
new Handle:cvar_ClusterId;
new Handle:cvar_QueryType;
new Handle:cvar_BlockRank;
new Handle:cvar_HideTks;

public Plugin:myinfo = {
	name = "Hidden Stats : Query Plugin",
	author = "iammitch",
	description = "A plugin designed to query the Hidden Stats database and pull out player rankings/scores.",
	version = PLUGIN_VERSION,
	url = "http://forum.hidden-source.com/showthread.php?11610-Stats-Revival"
};

/**
	Event that is fired when the plugin is started.
	Parameters: None
	Returns: Nothing
*/
public OnPluginStart() {
	
	LoadTranslations("common.phrases");

	// Hook into the say command
	RegConsoleCmd ( "say", Command_Say );
	RegConsoleCmd ( "say_team", Command_Say );

	// Cvars
	cvar_ServerId = CreateConVar(
		"hsm_stats_server_id", 
		"0",
		"The ID number of this server, used for stats tracking if the query type is set to 3.",
		_,
		true,
		0,
		false
	);

	cvar_ClusterId = CreateConVar(
		"hsm_stats_cluster_id", 
		"0",
		"The cluster ID number of this server, used for stats tracking if the query type is set to 4.",
		_,
		true,
		0,
		false
	);

	cvar_QueryType = CreateConVar(
		"hsm_stats_query_type", 
		"1",
		"The type of query to use for 'rank'. 1 = Look at Global, 2 = Look at Country, 3 = Look at Server, 4 = Look At Cluster.",
		_,
		true,
		1.0 ,
		true,
		4.0
	);

	cvar_BlockRank = CreateConVar(
		"hsm_stats_block_rank",
		"0",
		"Prevents the ranking of a player from being shown if this is set to 1. 0 = Show Rank. 1 = Don't Show Rank.",
		_,
		true,
		0.0,
		true,
		1.0
	);

	cvar_HideTks = CreateConVar(
		"hsm_stats_hide_tks",
		"0",
		"Prevents the players TK score from showing up in the 'stats' command. ",
		_,
		true,
		0.0,
		true,
		1.0
	);

	DatabaseInit();

}

public DatabaseInit() {
	
	new String:error[255];

	// db = SQL_DefConnect(error, sizeof(error));
	hndl_database = SQL_Connect ( "hsm_stats", true, error, sizeof ( error ) );

	if ( hndl_database == INVALID_HANDLE ) {
		PrintToServer("Could not connect: %s", error)
	}

}

/**
	Query the players rank.
*/
public QueryRank ( int:client, int:mode ) {
		
	decl String:steamID[32];
	GetClientAuthString ( client, steamID, sizeof(steamID) );

	decl String:db_query[512];

	if ( mode == QUERY_TYPE_GLOBAL ) {
		// Global stats, query client_stats for the data we need.
		Format ( 
			db_query, 	
			sizeof ( db_query ), 
			"CALL client_rank_global('%s');",
			steamID
		);
	}
	else if ( mode == QUERY_TYPE_COUNTRY ) {
		Format ( 
			db_query, 	
			sizeof ( db_query ), 
			"CALL client_rank_region('%s', %i);",
			steamID,
			GetConVarInt(cvar_ServerId)
		);
	}
	else if ( mode == QUERY_TYPE_CLUSTER ) {
		Format ( 
			db_query, 	
			sizeof ( db_query ), 
			"CALL client_rank_cluster('%s', %i);",
			steamID,
			GetConVarInt(cvar_ClusterId)
		);
	}
	else if ( mode == QUERY_TYPE_SERVER ) {
		// Server stats, query client_stats_servers for the data.
		Format (
			db_query,
			sizeof ( db_query ),
			"CALL client_rank_server ( '%s', %i );",
			steamID,
			GetConVarInt(cvar_ServerId)
		);
	}
	else {
		return;
	}

	SQL_TQuery ( hndl_database, Thread_QueryRankCallback, db_query, client );

}

/**
	Thread callback for querying the players ranking.
*/
public Thread_QueryRankCallback ( Handle:owner, Handle:handle, const String:error[], any:data ) {

	decl String:name[64];

	if ( !IsClientConnected ( data ) || !IsClientInGame ( data ) ) {
		return;
	}

	GetClientName ( data, name, sizeof(name) );

	if ( handle == INVALID_HANDLE ) {
		LogToGame ( "[Stats] DatabaseError: \"%s\"", error );
	}
	else if ( SQL_FetchRow ( handle ) ) {

		new Int:rank = SQL_FetchInt( handle, 0 );
		new Int:score = SQL_FetchInt( handle, 1 );

		decl String:buffer[512];
		if ( GetConVarBool ( cvar_BlockRank ) == false ) {
			Format ( buffer, sizeof(buffer), "\x03[Rank] %s is ranked #%i (%i Points)", name, rank, score );
		}
		else {
			Format ( buffer, sizeof(buffer), "\x03[Rank] %s has %i points", name, score );	
		}
		PrintToChatAll ( buffer );

	}
	else {
		PrintToChatAll ( "\x03[Rank] %s currently is not ranked on this server", name );
	}

}

/**
	Queries the players stats from the database.
	/-- Variables
	name - The player name to use.
	steamID - The SteamID to query.
*/
public QueryStats ( int:client, int:mode ) {

	decl String:steamID[32];
	GetClientAuthString ( client, steamID, sizeof(steamID) );

	decl String:db_query[512];

	if ( mode == QUERY_TYPE_GLOBAL ) {
		// Global stats, query client_stats for the data we need.
		Format ( 
			db_query, 	
			sizeof ( db_query ), 
			"CALL client_stats_global('%s');", 
			steamID
		);
	}
	else if ( mode == QUERY_TYPE_COUNTRY ) {
		// Country stats.
		Format (
			db_query,
			sizeof ( db_query ),
			"CALL client_stats_region('%s', %i);",
			steamID,
			GetConVarInt(cvar_ServerId)
		);
	}
	else if ( mode == QUERY_TYPE_CLUSTER ) {
		// Cluster stats.
		Format (
			db_query,
			sizeof ( db_query ),
			"CALL client_stats_cluster('%s', %i);",
			steamID,
			GetConVarInt(cvar_ClusterId)
		);
	}
	else if ( mode == QUERY_TYPE_SERVER ) {
		// Server stats, query client_stats_servers for the data.
		Format (
			db_query,
			sizeof ( db_query ),
			"CALL client_stats_server('%s', %i);",
			steamID,
			GetConVarInt(cvar_ServerId)
		);
	}
	else {
		return;
	}

	SQL_TQuery ( hndl_database, Thread_QueryStatsCallback, db_query, client );

}

/**
	Thread callback for querying the players stats.
*/
public Thread_QueryStatsCallback ( Handle:owner, Handle:result, const String:error[], any:data ) {

	decl String:name[64];

	if ( !IsClientConnected ( data ) || !IsClientInGame ( data ) ) {
		return;
	}

	GetClientName ( data, name, sizeof(name) );

	if ( result == INVALID_HANDLE ) {
		LogToGame ( "[Stats] DatabaseError: \"%s\"", error );
	}
	else if ( SQL_FetchRow ( result ) ) {

		new Int:db_kills = SQL_FetchInt( result, 0 );
		new Int:db_deaths = SQL_FetchInt( result, 1 );
		new Int:db_tks = SQL_FetchInt( result, 2 );
		new Int:db_suicides = SQL_FetchInt ( result, 3 );

		decl String:buffer[512];
		if ( GetConVarBool ( cvar_HideTks ) == false ) {
			Format ( buffer, sizeof(buffer), "\x03[Rank] %s: %i/%i/%i/%i (K/D/S/T)", name, db_kills, db_deaths, db_suicides, db_tks );
		}
		else {
			Format ( buffer, sizeof(buffer), "\x03[Rank] %s: %i/%i/%i (K/D/S)", name, db_kills, db_deaths, db_suicides );
		}

		PrintToChatAll( buffer );

	}
	else {
		PrintToChatAll ( "\x03[Rank] %s currently is not ranked on this server", name );
	}

}

public QueryTimePlayed ( int:client, int:mode ) {
	
	decl String:steamID[32];
	GetClientAuthString ( client, steamID, sizeof(steamID) );

	decl String:db_query[512];

	if ( mode == QUERY_TYPE_GLOBAL ) {
		// Global stats, query client_stats for the data we need.
		Format ( 
			db_query, 	
			sizeof ( db_query ), 
			"CALL client_timeplayed_global ( '%s' );", 
			steamID
		);
	}
	else if ( mode == QUERY_TYPE_COUNTRY ) {
		Format (
			db_query,
			sizeof ( db_query ),
			"CALL client_timeplayed_region('%s', %i);",
			steamID,
			GetConVarInt(cvar_ServerId)
		);
	}
	else if ( mode == QUERY_TYPE_CLUSTER ) {
		Format (
			db_query,
			sizeof ( db_query ),
			"CALL client_timeplayed_cluster('%s', %i);",
			steamID,
			GetConVarInt(cvar_ClusterId)
		);
	}
	else if ( mode == QUERY_TYPE_SERVER ) {
		// Server stats, query client_stats_servers for the data.
		Format (
			db_query,
			sizeof ( db_query ),
			"CALL client_timeplayed_server('%s', %i);",
			steamID,
			GetConVarInt(cvar_ServerId)
		);
	}
	else {
		return;
	}

	SQL_TQuery ( hndl_database, Thread_QueryTimePlayedCallback, db_query, client );

}

public Thread_QueryTimePlayedCallback ( Handle:owner, Handle:result, const String:error[], any:data ) {

	decl String:name[64];

	if ( !IsClientConnected ( data ) || !IsClientInGame ( data ) ) {
		return;
	}

	GetClientName ( data, name, sizeof(name) );

	if ( result == INVALID_HANDLE ) {
		LogToGame ( "[Stats] DatabaseError: \"%s\"", error );
	}
	else if ( SQL_FetchRow ( result ) ) {

		new Float:timePlayed = SQL_FetchFloat( result, 0 );

		decl String:buffer[512];
		Format ( buffer, sizeof(buffer), "\x03[Rank] %s: %.2f hours", name, timePlayed );		

		PrintToChatAll( buffer );

	}
	else {
		PrintToChatAll ( "\x03[Rank] %s currently is not ranked on this server", name );
	}

}

/*
	Hook into the 'say' and 'say_team' commands.
*/
public Action:Command_Say(client, argSize ) {

	argSize = 1;

	new String:arg1[64];
	new String:args[2][32];

	GetCmdArg ( 1, arg1, sizeof ( arg1 ) );
	argSize = ExplodeString ( arg1, " ", args, 2, 32, true );

	if ( strcmp ( args[0], QUERY_CMD_RANK, false ) == 0 ) {

		new target = -1;

		if ( argSize > 1 ) {

			/*
				More than one argument passed.
			*/
			target = FindTarget ( client, args[1], true, false );

			if ( target == -1 ) {
				return Plugin_Handled;
			}
		}
		else {
			target = client;
		}

		QueryRank ( target, GetConVarInt(cvar_QueryType) );

		return Plugin_Handled;

	}
	/*
	else if ( strcmp ( args[0], "ranks", false ) == 0 ) {

		for ( new i = 1; i <= MaxClients; i++ ) {

			if ( !IsClientConnected ( i ) && !IsClientInGame ( i ) ) {
				continue;
			}

			QueryRank ( i, GetConVarInt ( cvar_QueryType ) );

		}
		return Plugin_Handled;
	}
	*/
	/*
	else if ( strcmp ( args[0], "top", false ) == 0 ) {
	}
	*/
	else if ( strcmp ( args[0], QUERY_CMD_TIMEPLAYED, false ) == 0 ) {

		new target = -1;

		if ( argSize > 1 ) {
			/*
				More than one argument passed.
			*/
			target = FindTarget ( client, args[1], true, false );

			if ( target == -1 ) {
				return Plugin_Handled;
			}

		}
		else {
			// Just the sender.
			target = client;
		}

		QueryTimePlayed ( target, GetConVarInt(cvar_QueryType) );

		return Plugin_Handled;

	}
	else if ( strcmp ( args[0], QUERY_CMD_STATS, false ) == 0 ) {

		new target = -1;

		if ( argSize > 1 ) {
			/*
				More than one argument passed.
			*/
			target = FindTarget ( client, args[1], true, false );

			if ( target == -1 ) {
				return Plugin_Handled;
			}
		}
		else {
			// Just the sender.
			target = client;
		}

		QueryStats ( target, GetConVarInt(cvar_QueryType) );

		return Plugin_Handled;

	}

	return Plugin_Continue;
}
